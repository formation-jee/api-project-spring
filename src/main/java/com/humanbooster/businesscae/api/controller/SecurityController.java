package com.humanbooster.businesscae.api.controller;

import com.humanbooster.businesscae.api.model.Candidat;
import com.humanbooster.businesscae.api.model.User;
import com.humanbooster.businesscae.api.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/security")
public class SecurityController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;


    @PostMapping(value = "/register")
    void saveCandidat(@Valid @RequestBody User user) {
        if(userRepository.findByUsername(user.getUsername()) == null ){

            String encodedPassword = this.passwordEncoder.encode(user.getPassword());

            user.setPassword(encodedPassword);
            userRepository.save(user);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Utilisateur connu");
        }
    }


}
