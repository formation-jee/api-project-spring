package com.humanbooster.businesscae.api.controller;

import com.humanbooster.businesscae.api.model.Candidat;
import com.humanbooster.businesscae.api.repository.CandidatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/candidats")
public class CandidatController {

    @Autowired
    private CandidatRepository candidatRepository;

    @GetMapping(value = "")
    List<Candidat> getAll(){
       return this.candidatRepository.findAll();
    }

    @GetMapping(value = "{candidat}")
    Candidat getOne(@PathVariable(name="candidat", required = false) Candidat candidat){
        if(candidat == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Candidat inconnu !");
        }
        return candidat;
    }

    @PostMapping(value = "")
    ResponseEntity<Candidat> saveCandidat(@Valid @RequestBody Candidat candidat) {
        this.candidatRepository.save(candidat);
        return new ResponseEntity<Candidat>(candidat, HttpStatus.CREATED);
    }

    @PutMapping(value = "{candidat}")
    ResponseEntity<Candidat> updateCandidat(@Valid @RequestBody Candidat candidatUpdate,
                                            @PathVariable(name="candidat", required = false) Candidat candidat){
        if(candidat == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Candidat inconnu !");
        } else {
            candidatUpdate.setId(candidat.getId());
            this.candidatRepository.save(candidatUpdate);
            return new ResponseEntity<Candidat>(candidatUpdate, HttpStatus.OK);
        }
    }

    @DeleteMapping(value = "{candidat}")
    void deleteCandidat(@PathVariable(name = "candidat", required = false) Candidat candidat){
        if(candidat == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "candidat inconnu !");
        }else {
            candidatRepository.delete(candidat);
        }
    }

}
