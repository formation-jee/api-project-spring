package com.humanbooster.businesscae.api;

import com.humanbooster.businesscae.api.model.Candidat;
import com.humanbooster.businesscae.api.repository.CandidatRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Configuration
public class LoadData {
    private static final Logger logger = LoggerFactory.getLogger(LoadData.class);

    @Bean
    CommandLineRunner initDatabase(CandidatRepository candidatRepository) throws ParseException {
        logger.info("Lancement du préchargement des données");
        if(candidatRepository.count() == 0){
            logger.info("Création d'un candidat !!!!");
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date birthAurelien = formatter.parse("30/03/1993");

            Candidat candidat = new Candidat("Delorme", "Aurélien", birthAurelien, "Place de Jaude","Clermont", "63000");

            Candidat candidat2 = new Candidat("Toto", "Toto", birthAurelien, "Ailleurs","Paris", "75000");

            return args -> {
                logger.info("Preloading " + candidatRepository.save(candidat));
                logger.info("Preloading " + candidatRepository.save(candidat2));
            };

        } else {
            return args -> {
                logger.info("Data already load");
            };
        }
    }
}
