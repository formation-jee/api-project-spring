package com.humanbooster.businesscae.api.repository;

import com.humanbooster.businesscae.api.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {

    @Override
    List<User> findAll();

    User findByUsername(String username);

}
